#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Tkinter import *
import sys, re

WIDTH=640
HEIGHT=480
DEBUG=False

root = Tk()
root.title("Gdemo")
root.geometry('%dx%d' % (WIDTH, HEIGHT))
canvas = Canvas(root, width = WIDTH, height = HEIGHT)

moveTo = re.compile(r"^MOVETO\s+(\d{1,3})\s*,\s*(\d{1,3})$", re.I)
color = re.compile(r"^COLOR\s+#([0-9A-F]{6})$", re.I)
drawTo = re.compile(r"^DRAWTO\s+(\d{1,3})\s*,\s*(\d{1,3})(\s+THROUGH\s+(\d{1,3})\s*,\s*(\d{1,3}))?$", re.I)

pos = (0, 0, 0xFFFFFF)

def log(str):
    if DEBUG:
        print str

def handleMoveTo(line, pos):
    res = moveTo.match(line)
    (x, y, c) = pos
    if res:
        log('Since ' + str(x) + ', ' + str(y) + '. Color = #' + '{:06X}.'.format(c))
        x = int(res.group(1))
        y = int(res.group(2))
        log('Move to ' + str(x) + ', ' + str(y))
        return (x, y, c)
	
def handleDrawTo(line, pos):
    res = drawTo.match(line)
    (x, y, c) = pos
    if res:
        log('Since ' + str(x) + ', ' + str(y) + '. Color = #' + '{:06X}.'.format(c))
        xTo = int(res.group(1))
        yTo = int(res.group(2))
        xThrough = -1
        yThrough = -1
        #if len(res.groups()) > 3:
        #	xThrough = int(res.group(4))
        #	yThrough = int(res.group(5))
        log('Draw to ' + str(xTo) + ', ' + str(yTo) + ' through ' + str(xThrough) + ', ' + str(yThrough))
        if xThrough >= 0 and yThrough >= 0:
            #canvas.create_arc(x, y, xTo, yTo, style = 'arc', outline='#' + '{:06X}'.format(c))
            canvas.create_line(x, y, xThrough, yThrough, xTo, yTo, smooth = True, splinesteps = 2, fill='#' + '{:06X}'.format(c))
        else:
            canvas.create_line(x, y, xTo, yTo, smooth = False, width=3, fill='#' + '{:06X}'.format(c))
        return (xTo, yTo, c)
	
def handleColor(line, pos):
    res = color.match(line)
    (x, y, c) = pos
    if res:
        log('Since ' + str(x) + ', ' + str(y) + '. Color = #' + '{:06X}.'.format(c))
        c = int(res.group(1), 16)
        log('Change color to #' + '{:06X}.'.format(c))
        return (x, y, c)

def parseLine(line, pos):
    line = line.strip()
    if not line:
        return pos
    res = handleMoveTo(line, pos)
    if res:
        return res
    res = handleDrawTo(line, pos)
    if res:
        return res
    res = handleColor(line, pos)
    if res:
        return res

def close():
    root.destroy()
    root.quit()

for line in sys.stdin:
    pos = parseLine(line, pos)
    if not pos:
        log('Error!')
        print 'Error!'
        exit(1)

canvas.pack()
root.mainloop()
